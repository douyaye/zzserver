package zzserver

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"os/signal"
	"sync"
	"time"
)

type Server struct {
	router IRouter

	//监听的端口
	wsPort  int
	tcpPort int

	connected       chan *Client              //刚进入的,放入管道
	disconnected    chan *Client              //退出的,放入管道
	clients         map[int]*Client           // [登陆序号]客户端
	onlineNumber    int                       //在线人数
	broadcast       chan []byte               //广播消息
	ConnectionIndex int64                     //连接计数
	chanRange       chan func(c *Client) bool //
	chanClose       chan struct{}             //
	wg              *sync.WaitGroup           //

	ginEngine   *gin.Engine
	wsPath      string
	messageType int
}

func NewZZServer() *Server {
	s := &Server{
		clients:      make(map[int]*Client),
		broadcast:    make(chan []byte, 64),
		connected:    make(chan *Client, 64),
		disconnected: make(chan *Client, 64),
		chanRange:    make(chan func(c *Client) bool, 64),
		chanClose:    make(chan struct{}, 8),
		wg:           &sync.WaitGroup{},
		messageType:  websocket.TextMessage,
	}
	go s.run()
	return s
}

// run 开始
func (h *Server) run() {
	defer func() {
		h.wg.Done()
		log.Println("[zzserver] 运行结束")
	}()
	h.wg.Add(1)
	for {
		select {
		case client := <-h.connected: //用户连接
			h.clients[client.ConnectionIndex] = client
			//log.Println("用户 进入:"+client.GetRemoteAddr()+",当前登录:", h.ClientsNum, ",未登录:", len(h.ClientsNotloggedin))
			h.onlineNumber++
		case client := <-h.disconnected: //用户断开连接
			client.Close()
			if _, ok := h.clients[client.ConnectionIndex]; ok {
				delete(h.clients, client.ConnectionIndex)
				h.onlineNumber--
			}
			// log.Println("当前人数:", h.ClientsNum)
			// log.Print("用户 断开:"+client.GetRemoteAddr()+",当前登录:", len(h.clients), "未登录:", len(h.ClientsNotloggedin), "\r\n")
		case message := <-h.broadcast: //广播消息
			//var socketMsg = packet(message) //socket先封装, 再发送
			for _, client := range h.clients {
				//client.sendByteWithNoPacket(message, socketMsg)
				client.SendByte(message)
			}
		case f := <-h.chanRange:
			for _, client := range h.clients {
				if !f(client) {
					break
				}
			}
		case <-h.chanClose:
			return
		}
	}
}

func (h *Server) SetRouter(r IRouter) {
	h.router = r
}

// SetMessageType websocket的协议类型,1:text, 2:binary
//
//	默认值：websocket.TextMessage
//	可选： websocket.BinaryMessage
func (h *Server) SetMessageType(t int) {
	h.messageType = t
}

// SendToAll 广播消息
func (h *Server) SendToAll(message []byte) {
	h.broadcast <- message
}

// SendToAllJson 广播消息
func (h *Server) SendToAllJson(obj interface{}) {
	b, err := json.Marshal(obj)
	if err != nil {
		return
	}
	h.broadcast <- b
}

// startTcpSocket 开启socket监听
func (h *Server) startTcpSocket() {
	//开启普通socket监听
	serverSocket(h, fmt.Sprintf(":%d", h.tcpPort))
}

//func (h *Server) startWebsocketAndHttp() {
//	//开启普通websocket监听
//	defer func() {
//		log.Fatalln("[zzserver] websocket监听 已经退出!")
//	}()
//
//	if h.wsPath == "" {
//		h.wsPath = "/"
//	}
//	http.HandleFunc(h.wsPath, func(w http.ResponseWriter, r *http.Request) {
//		log.Println("[zzserver] r.URL.Path:", r.URL.Path)
//		log.Println("[zzserver] h.wsPath:", h.wsPath)
//		if r.URL.Path != h.wsPath {
//			return
//		}
//		serveWs(h, w, r)
//	})
//	err := http.ListenAndServe(fmt.Sprintf(":%d", h.wsPort), nil)
//	if err != nil {
//		log.Fatal("[zzserver] websocket启动失败! " + err.Error())
//	}
//
//	//var err error
//	//if !zzcfg.Wss {
//	//	err = http.ListenAndServe(websocketaddr, nil)
//	//} else {
//	//	//err := http.ListenAndServeTLS(":443", "server.crt", "server.pem", nil)
//	//	log.Println("server run WSS")
//	//	err = http.ListenAndServeTLS(websocketaddr, zzcfg.Wss_cert, zzcfg.Wss_key, nil)
//	//}
//}

func (h *Server) SetGinEngine(g *gin.Engine) {
	h.ginEngine = g
}
func (h *Server) SetWebsocketPort(port int) {
	h.wsPort = port
}
func (h *Server) SetTCPPort(port int) {
	h.tcpPort = port
}
func (h *Server) SetWsPath(path string) {
	h.wsPath = path
}

func (h *Server) startGinWebsocket() {
	//g := gin.Default()
	//g.Any("/", h.serveWsGin)
	if h.ginEngine == nil {
		gin.SetMode("release")
		h.ginEngine = gin.New()
		h.ginEngine.Use(gin.Recovery())
		log.Println("[zzserver] 未设置ginEngine, 自动生成 gin")
	}
	if h.wsPath == "" {
		h.wsPath = "/"
	} else {
		log.Println("[zzserver] websocket 绑定路径:", h.wsPath)
	}
	h.ginEngine.GET(h.wsPath, func(c *gin.Context) {
		//log.Println("[zzserver] r.URL.Path:", c.Request.URL.Path)
		//log.Println("[zzserver] h.wsPath:", h.wsPath)
		if c.Request.URL.Path != h.wsPath {
			return
		}
		startWs(h, c.Writer, c.Request)
	})
	err := h.ginEngine.Run(fmt.Sprintf(":%d", h.wsPort))
	if err != nil {
		log.Fatal("[zzserver] websocket启动失败! " + err.Error())
		return
	}
}

func (h *Server) Start() {
	if h.router == nil {
		log.Fatalln("[zzserver] please set router use server.SetRouter()")
		return
	}
	if h.wsPort <= 0 && h.tcpPort <= 0 {
		log.Fatalln("[zzserver] 请设置websocket端口或者tcp端口")
		return
	}

	if h.wsPort > 0 {
		log.Printf("[zzserver] Websocket 启动成功 at :%d", h.wsPort)
		go h.startGinWebsocket()
	}
	if h.tcpPort > 0 {
		fmt.Printf("[zzserver] TCP 启动成功  at :%d", h.tcpPort)
		go h.startTcpSocket()
	}

}

func (h *Server) Close() {
	if h.router != nil {
		h.router.OnServerClose()
	}
	h.chanClose <- struct{}{}
	h.wg.Wait()
}

func (h *Server) WaitCloseSignal(before, after func()) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c
	log.Println("[zzserver] 收到关闭信号")
	if before != nil {
		log.Println("[zzserver] 执行关闭前回调 before()")
		before()
	}
	log.Println("[zzserver] 执行Close()")
	h.Close()
	if after != nil {
		log.Println("[zzserver] 执行关闭后回调 after()")
		after()
	}
	time.Sleep(500 * time.Millisecond)
	log.Println("[zzserver] 服务器关闭 再见~ ")
}

func (h *Server) Online() int {
	return h.onlineNumber
}

// Range 在同一个go中执行, 不要执行很久的操作
func (h *Server) Range(f func(c *Client) bool) {
	select {
	case h.chanRange <- f:
	default:
		log.Println("[zzserver] Range ERROR:  chan is full")
	}
}
