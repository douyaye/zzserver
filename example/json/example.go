package main

import (
	"fmt"
	"gitee.com/douyaye/zzserver"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
	//"github.com/bean0815/zzserver"
	//"gitee.com/douyaye/zzserver"
)

func main() {
	//开启服务
	srv := zzserver.NewZZServer()

	go func() {
		t := time.Tick(5 * time.Second)
		for {
			<-t
			n := time.Now()
			srv.Range(func(c *zzserver.Client) bool {
				if n.Sub(c.LastMsgTime) > 10*time.Second {
					c.Close()
				}
				return true
			})
		}
	}()

	gin.SetMode("release")
	g := gin.Default()
	g.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"code": 0})
	})

	srv.SetGinEngine(g)
	srv.SetRouter(&P{})        //绑定路由接口
	srv.SetWebsocketPort(9999) //websocket端口
	srv.SetWsPath("/gamefish")
	srv.Range(func(c *zzserver.Client) bool {
		fmt.Println(c.LastMsgTime)
		return true
	})

	//srv.SetTCPPort(9988) //不设置就不会启动监听
	srv.Start()

	srv.WaitCloseSignal(func() {
		log.Println("关闭前")
	}, func() {
		log.Println("关闭后")
	})
}

type P struct {
	zzserver.BaseRouter
}

// OnMessage 接收客户端发送的消息
func (p *P) OnMessage(c *zzserver.Client, message []byte) {
	if string(message) == "close" {
		c.Server.SendToAll([]byte(fmt.Sprintf("user%d closed", c.ConnectionIndex)))
		c.Close()
	} else {
		c.Server.SendToAll([]byte(fmt.Sprintf("user%d say: %s", c.ConnectionIndex, string(message))))
	}
}

// OnDisconnect 客户端断开
func (p *P) OnDisconnect(c *zzserver.Client) {
}
func (p *P) OnServerClose() {
	//log.Println("OnServerClose.....")
	//time.Sleep(3 * time.Second)
	//log.Println("OnServerClose.....")
}
