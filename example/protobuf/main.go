package main

import (
	"encoding/json"
	"gitee.com/douyaye/zzserver/example/protobuf/pro/pb"
	"github.com/golang/protobuf/proto"
	"log"
)

func main() {
	loginArg := &pb.Login{
		Uid:   111,
		Token: "build_gitee_com_douyaye_zzserver_example_aye_zzserver_example_protobuf.exe -gcflags",
		Pid:   1,
	}
	data, _ := warp("login", loginArg)

	dataJson, _ := json.Marshal(loginArg)

	log.Println("data:", len(data))
	log.Println("dataJson:", len(dataJson))
	log.Println("开始解码")

	t, da, err := unWarp(data)
	if err != nil {
		return
	}

	log.Println("协议:", t)
	var tmp = new(pb.Login)
	proto.Unmarshal(da, tmp)
	log.Printf(" %+v", tmp)

	//w := new(pb.Wrapper)
	//
	//err := proto.Unmarshal(data, w)
	//if err != nil {
	//	log.Println(err)
	//	return
	//}
	//
	//log.Println("协议号:", w.Name)
	//
	//person := new(pb.Person)
	//err = proto.Unmarshal(w.Data, person)
	//if err != nil {
	//	log.Println(err)
	//	return
	//}
	//
	//log.Printf("person: %+v", person)
}

func warp(t string, obj proto.Message) (data []byte, err error) {
	var objData []byte
	objData, err = proto.Marshal(obj)
	if err != nil {
		return
	}
	w := &pb.Wrapper{
		Name: t,
		Data: objData,
	}
	data, err = proto.Marshal(w)
	return
}

func unWarp(b []byte) (t string, data []byte, err error) {
	w := new(pb.Wrapper)
	err = proto.Unmarshal(b, w)
	if err != nil {
		return
	}
	t = w.Name
	data = w.Data
	return
}
