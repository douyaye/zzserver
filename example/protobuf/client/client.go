package main

import (
	"gitee.com/douyaye/zzserver/example/protobuf/pro"
	"gitee.com/douyaye/zzserver/example/protobuf/pro/pb"
	"gitee.com/douyaye/zzserver/example/protobuf/server/protocol"
	"github.com/gorilla/websocket"
	"log"
)

var (
	sendChan = make(chan []byte, 128)
)

func main() {

	client()

	//p := &pb.Person{
	//	Name: "lzq",
	//}

	arg := &pb.Login{
		Uid:   12345,
		Token: "token",
		Pid:   1,
	}

	warp, err := protocol.ProtobufWarp(pro.TLogin, arg)
	if err != nil {
		return
	}

	//data, err := proto.Marshal(p)
	//if err != nil {
	//	log.Fatal(err)
	//}

	sendChan <- warp

	//fmt.Printf("matshal data len: %d\n", len(data))
	//
	//np := &pb.Person{}
	//if err = proto.Unmarshal(data, np); err != nil {
	//	log.Fatal(err)
	//}
	//
	//fmt.Printf("unmatshal person name: %s\n", np.Name)

	select {}

}

func client() {
	urlStr := "ws://127.0.0.1:9999?messageType=binary"
	c, _, err := websocket.DefaultDialer.Dial(urlStr, nil)
	if err != nil {
		log.Fatal("dial:", err)
		return
	}
	//defer c.Close()
	log.Println("WS connect success:", urlStr)
	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err2 := c.ReadMessage()
			if err2 != nil {
				log.Println("read:", err2)
				return
			}
			log.Println("message:len:", len(message))
			//np := &pb.Login{}
			//if err := proto.Unmarshal(message, np); err != nil {
			//	log.Println("解析proto数据失败~~~~~~~~")
			//	//log.Fatal(err)
			//}
			//log.Printf("收到数据: %+v", np)
		}
	}()

	go func() {
		for {
			msg := <-sendChan
			c.WriteMessage(websocket.BinaryMessage, msg)
		}
	}()

}
