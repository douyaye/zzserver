package protocol

import (
	"encoding/json"
	"errors"
	"gitee.com/douyaye/zzserver/example/protobuf/pro/pb"
	"github.com/golang/protobuf/proto"
	"log"
)

func JsonUnWarp(message []byte) (t string, data []byte, err error) {
	var result map[string]json.RawMessage
	if err = json.Unmarshal(message, &result); err != nil {
		return
	}
	tmpT, ok := result["t"]
	if !ok {
		err = errors.New("not exists key")
		return
	}
	err = json.Unmarshal(tmpT, &t)
	if err != nil {
		log.Println("err=>", err)
		return
	}
	data = result["d"]
	return
}

func JsonWarp(t string, obj proto.Message) (data []byte, err error) {
	m := map[string]any{
		"t": t,
		"d": obj,
	}
	return json.Marshal(m)
}

func ProtobufWarp(t string, obj proto.Message) (data []byte, err error) {
	var objData []byte
	objData, err = proto.Marshal(obj)
	if err != nil {
		return
	}
	w := &pb.Wrapper{
		Name: t,
		Data: objData,
	}
	data, err = proto.Marshal(w)
	return
}

func ProtobufUnWarp(b []byte) (t string, data []byte, err error) {
	w := new(pb.Wrapper)
	err = proto.Unmarshal(b, w)
	if err != nil {
		return
	}
	t = w.Name
	data = w.Data
	return
}
