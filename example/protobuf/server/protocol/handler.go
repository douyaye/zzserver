package protocol

import (
	"gitee.com/douyaye/zzserver"
	"log"
)

var (
	handlerMap = make(map[string]*sHandler)
)

type sHandler struct {
	t string
	f func(*zzserver.Client, []byte)
}

func RegHandler(t string, f func(*zzserver.Client, []byte)) {
	if _, ok := handlerMap[t]; ok {
		log.Println("err: 重复注册 handler <" + t + "> ")
		return
	}
	handlerMap[t] = &sHandler{t: t, f: f}
}
