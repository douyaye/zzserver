package protocol

import (
	"gitee.com/douyaye/zzserver"
	"log"
)

type P struct {
}

func (p P) OnMessage(c *zzserver.Client, message []byte) {
	var t string    //协议
	var data []byte //内容
	var err error

	//提取协议号
	if c.WsMessageType == 1 {
		t, data, err = JsonUnWarp(message)
	} else {
		t, data, err = ProtobufUnWarp(message)
	}
	if err != nil {
		//格式错误
		log.Println("格式错误")
		return
	}

	//执行
	if v, ok := handlerMap[t]; ok {
		v.f(c, data)
		return
	}
	log.Println("未注册协议:" + t)
}

func (p P) OnDisconnect(c *zzserver.Client) {
	//run(c, TDisconnect, nil)
}

func (p P) OnServerClose() {
	log.Println("OnServerClose")
	//serverClosing = true
	//run(nil, TServerClose, nil)
}

func (p P) OnServerUpdate() {
}

func (p P) OnConnected(c *zzserver.Client) {
	//run(c, TConnected, nil)
}
