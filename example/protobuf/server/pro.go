package main

import (
	"encoding/json"
	"gitee.com/douyaye/zzserver"
	"gitee.com/douyaye/zzserver/example/protobuf/pro"
	"gitee.com/douyaye/zzserver/example/protobuf/pro/pb"
	"gitee.com/douyaye/zzserver/example/protobuf/server/protocol"
	"github.com/golang/protobuf/proto"
	"log"
)

func RegHandlerDetail() {
	protocol.RegHandler(pro.TLogin, OnLogin)

}
func UnmarshalAll(client *zzserver.Client, data []byte, t proto.Message) (err error) {
	if client.WsMessageType == 1 {
		err = json.Unmarshal(data, t)
	} else {
		err = proto.Unmarshal(data, t)
	}
	return
}

func OnLogin(client *zzserver.Client, data []byte) {
	t := new(pb.Login)
	err := UnmarshalAll(client, data, t)
	if err != nil {
		return
	}
	log.Printf("%+v", t)
}
