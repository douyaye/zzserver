package main

import (
	//"github.com/bean0815/zzserver"
	"gitee.com/douyaye/zzserver"
	"gitee.com/douyaye/zzserver/example/protobuf/server/protocol"
)

func main() {

	RegHandlerDetail()

	//开启服务
	srv := zzserver.NewZZServer()

	//go func() {
	//	t := time.Tick(5 * time.Second)
	//	for {
	//		<-t
	//		n := time.Now()
	//		srv.Range(func(c *zzserver.Client) bool {
	//			if n.Sub(c.LastMsgTime) > 10*time.Second {
	//				c.Close()
	//			}
	//			return true
	//		})
	//	}
	//}()

	srv.SetRouter(&protocol.P{}) //绑定路由接口
	srv.SetWebsocketPort(9999)   //websocket端口
	srv.SetTCPPort(9988)         //不设置就不会启动监听
	//srv.SetMessageType(websocket.BinaryMessage)
	srv.Start()
}

//type P struct {
//	zzserver.BaseRouter
//}
//
//// OnMessage 接收客户端发送的消息
//func (p *P) OnMessage(c *zzserver.Client, message []byte) {
//	if string(message) == "close" {
//		c.Server.SendToAll([]byte(fmt.Sprintf("user%d closed", c.ConnectionIndex)))
//		c.Close()
//		return
//	}
//
//	//c.WsMessageType == websocket.BinaryMessage
//
//	np := &pb.Person{}
//	if err := proto.UnmarshalAll(message, np); err != nil {
//		log.Println("解析proto数据失败~~~~~~~~")
//		//log.Fatal(err)
//	} else {
//
//		log.Printf("解析proto数据 成功!!!! %+v", np)
//		np.Name = "welcome " + np.Name
//		data, err := proto.Marshal(np)
//		if err != nil {
//			log.Println("proto.Marshal失败~~~~~~~~")
//		}
//		c.SendByte(data)
//	}
//
//	//c.Server.SendToAll([]byte(fmt.Sprintf("user%d say: %s", c.ConnectionIndex, string(message))))
//
//}
//
//// OnDisconnect 客户端断开
//func (p *P) OnDisconnect(c *zzserver.Client) {
//}
//func (p *P) OnServerClose() {
//	log.Println("OnServerClose.....")
//	time.Sleep(3 * time.Second)
//	log.Println("OnServerClose.....")
//}
//func (b *P) OnConnected(c *zzserver.Client) {
//	log.Println("有用户连接...")
//}
