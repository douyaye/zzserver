# zzServer

该项目是一个简易的socket服务端程序，
1. 支持webSocket以及普通tcpSocket
2. websocket支持`BinaryMessage`和`TextMessage`,且能支持同时解析`json`和`protobuf`协议
3. websocket同时支持gin


 
  
### 使用示例

- 服务端
```

//开启服务
srv := zzserver.NewZZServer()

//设置gin,不设置会生成一个默认的
g := gin.Default()
g.GET("/hello", func(c *gin.Context) {
  c.JSON(http.StatusOK, gin.H{"code": 0})
})

srv.SetGinEngine(g)          //设置gin
srv.SetRouter(&protocol.P{}) //绑定路由接口
srv.SetWebsocketPort(9999)   //websocket端口
srv.SetTCPPort(9988)         //不设置就不会启动监听
srv.SetMessageType(websocket.TextMessage)   //设置服务端返回消息类型， 默认值：websocket.TextMessage 可选：websocket.BinaryMessage
srv.Start()


//遍历客户端

srv.Range(func(c *zzserver.Client) bool {
	fmt.Println(c.LastMsgTime)
	return true
})

```
 
- 客户端
```
ws://127.0.0.1:9999?messageType=text  发送字符串消息 (默认值)
ws://127.0.0.1:9999?messageType=binary   发送binary消息
http://127.0.0.1:9999/hello             http访问
```



请参考 `example`

喜欢请 start 一下


by. `douya`
 